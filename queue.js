let collection = [];

// collection.push("name");
// console.log(collection)
// Write the queue functions below.


function print() {
    //Print queue elements.
    return collection;
}
// console.log(print())


function enqueue(student) {
   //first approach
    collection.push(student);
    return collection

    //OTHER
    // if( collection.length <= 0 ){
    //     collection[0] =student

    
    // } else {
    //     const index = collection.length
    //     collection[index] = student
    // }
    // return collection
 
}

function dequeue() {
    //1st way
    collection.shift()
    return collection

    //2nd way
    // collection = collection.slice(1)
    // return collection;

    //3rd
    // let newArray = []

    // for(let i = 1; i < collection.length; i++){

    //     newArray[i-1] = collection[i]
    // }
    // return newArray

}

function front(){

    return collection[0]
}

function size() {
    return collection.length
}

function isEmpty() {
    return collection.length == 0
}

module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};